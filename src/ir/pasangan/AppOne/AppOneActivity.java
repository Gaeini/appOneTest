package ir.pasangan.AppOne;

import java.util.ArrayList;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;


public class AppOneActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Log.i("Man", "" + getDivision(20));

    }


    public ArrayList<Integer> getDivision(Integer a)
    {
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        for (int i = 1; i <= a; i++)
        {
            if (a % i == 0)
            {

                numbers.add(i);
            }
        }
        return numbers;
    }

}